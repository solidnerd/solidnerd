# Hi there 👋
## Glad to see you here! 🤩

I'm a Senior DevOps Engineer at [Aidence](https://aidence.com), building and running Infrastructure for AI-powered clinical applications for the lung cancer pathway.

I became a GitLab Hero  🦊 to support the community into all kinds where my knowledge can help.

In my offtime, I have an ambition for Personal Growth. 📈🦆  and outdoor Activies like 🥾 or ✈️

If you want to have a deeper look into my [Polywork Account](https://log.solidnerd.dev) to see my latest Activities
